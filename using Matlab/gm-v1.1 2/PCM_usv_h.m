% Copyright CAJOE tech(www.361ic.net)
% Author:Z.Li 2014.06


clear all;
close all;


%[Y,FS,NBITS] = wavread('E:\Code_Matlab\data\GM-V1.1\0703-3.wav');
[Y,FS,NBITS] = wavread('.\0703-3.wav');

c = 3E8; %(m/s) speed of light

%radar parameters
Tp = 0.250; %(s) pulse time
N = Tp*FS; %# of samples per pulse

Sig=Y(:,1);
figure;
sigLength=length(Sig);
t=(0:(sigLength-1))/FS;
subplot(211);plot(t,Sig);xlabel('Time(s)')

timeWindow=0.02;%s
MagValid=0.5;% relative 


for i=1:sigLength;
    if(abs(Sig(i))>MagValid)
        Sig(i)=abs(Sig(i));
    else
        Sig(i)=0;
    end
end
subplot(212);plot(t,Sig);xlabel('Time(s)')


%Y_med = medfilt1(Sig,50);
%subplot(313);plot(t,Y_med);xlabel('Time(s)');
Y_med=Sig;




cube_1=zeros(sigLength,1);
counters=0;
counters_PerTimeZone=0;
for i=5:sigLength;
    if((Y_med(i))>0)&&((Y_med(i-1))==0)&&((Y_med(i-2))==0)&&((Y_med(i-3))==0)
        %Determine it is one pluse
        cube_1(i)=1;
        counters=counters+1;

    end
end
figure;
subplot(211);plot(t,cube_1,'b.');
xlabel('Time(s)');ylabel('counter pluse');
axis([0 25 0 2]);
CPM=counters*60/(max(t))


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%分时段显示CPM
digiSamples=4*FS;  %每4秒刷新一次CPM数值，



mRefresh=max(t)/4;
mRefreshDown=fix(max(t)/4);
mRefreshUp=ceil(max(t)/4);
MemCPM=zeros(mRefreshUp,1); %存储CPM数值的容器

MemTimes=zeros(mRefreshUp,1);
for i=1:1:mRefreshUp
    MemTimes(i)=4*i;
end

counters=0;
if (digiSamples<sigLength)
    for iTimeZones=1:1:mRefreshDown

            %在一个时间段内计数
            for i=((iTimeZones-1)*digiSamples+5):iTimeZones*digiSamples;
                if((Y_med(i))>0)&&((Y_med(i-1))==0)&&((Y_med(i-2))==0)&&((Y_med(i-3))==0)
                    %Determine it is one pluse
                    cube_1(i)=1;
                    counters=counters+1;

                end
            end

      MemCPM(iTimeZones)=counters;
      counters=0;
    end
end

MemCPM=MemCPM*60/4;

uSVCPM=MemCPM./151;

subplot(212);plot(MemTimes,uSVCPM);
xlabel('Time(s)');
ylabel('uSV/h');

axis([0 30 0 10]);



title('Geiger counter Kits(manufacturer:CAJOE)');